package ru.tinkoff.java2scala._0_helloworld

import org.scalatest.{FlatSpec, Matchers}

class HelloWorldTest extends FlatSpec with Matchers {

  behavior of "HelloWorldTest"

  it should "return HelloWorld in scala" in {
    HelloWorld.howToWriteHelloWorldInScala shouldBe """|object Main extends App {
                                                       |  println("Hello world!")
                                                       |}
                                                       |""".stripMargin
  }

}
